package main

import (
	"emailer"
	"encoding/json"
	"errors"
	"flag"
	"os"
)

func main() {

	configFileLocation := flag.String("configFile", "/home/deploy/godir/bin/emailConfig.json", "Location of Configuration File For Emailer")
	sendTo := flag.String("to", "", "Emaill Address To Send To")
	subject := flag.String("subject", "", "Subject of Email")
	message := flag.String("message", "", "Message To Send Out")
	flag.Parse()

	err := checkInputs(sendTo, subject, message)
	if err != nil {
		panic(err)
	}

	file, err := os.Open(*configFileLocation)
	if err != nil {
		panic(err)
	}

	decoder := json.NewDecoder(file)
	configuration := Configuration{}
	err = decoder.Decode(&configuration)
	if err != nil {
		panic(err)
	}

	emailer := emailer.GetEmailer(configuration.Username, configuration.Password, configuration.Host, configuration.Port)

	err = emailer.SendEmail(*sendTo, *subject, *message)
	if err != nil {
		panic(err)
	}

}

func checkInputs(sendTo *string, subject *string, message *string) error {
	if sendTo == nil || *sendTo == "" {
		return errors.New("Must Set Reciever Email Address With the -to Flag")
	}

	return nil
}

type Configuration struct {
	Username string
	Password string
	Host     string
	Port     string
}
